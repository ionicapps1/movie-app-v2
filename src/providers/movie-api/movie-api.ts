import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {IMovie} from "../../interface/IMovie";
import {Platform} from "ionic-angular";
import {Observable} from "rxjs/Rx";

/*
  Generated class for the MovieApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieApiProvider {

  private baseUrl: string = "http://localhost:1818/movies/";
  public pages = 1;
  private searchpages = 1;

  movies: IMovie[];

  constructor(private readonly http: HttpClient, private readonly platform: Platform) {
  }


  addMovie(data): Observable<any> {
    const postData = {
      'popularity': data.popularity,
      'vote_count': data.vote_count,
      'video': data.video,
      'poster_path': data.poster_path,
      'adult': data.adult,
      'backdrop_path': data.backdrop_path,
      'original_language': data.original_language,
      'original_title': data.original_title,
      'genre_ids': "{" + data.genre_ids + "}",
      'title': data.title,
      'vote_average': data.vote_average,
      'overview': data.overview,
      'release_date': data.release_date
    }
    return this.http.post(this.baseUrl + "addMovie", postData)
  }

  getMovies(): Observable<any> {
    //return this.http.get(this.baseUrl + "movie/popular?api_key=ab02b2d5b97666e564bd2ece7bb1494c&language=fr&page=" + this.pages);
    return this.http.get(this.baseUrl + "getMovies?page=" + this.pages);
    //return this.http.get(`${this.baseUrl}`);
  }

  getNextMovies(): Observable<any> {
    this.pages++;
    this.searchpages = 1;
    return this.http.get(this.baseUrl + "getMovies?page=" + this.pages);
  }

  searchMovie(search, inc): Observable<any> {
    if (inc) {
      this.searchpages++;
    }
    console.log(this.searchpages);
    //return this.http.get(this.baseUrl + "search/movie?language=fr&query=" + search + "&page=" + this.searchpages);
  }
}
