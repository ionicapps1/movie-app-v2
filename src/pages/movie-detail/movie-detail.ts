import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {IMovie} from "../../interface/IMovie";
import {FavoriteMovieProvider} from "../../providers/favorite-movie/favorite-movie";

/**
 * Generated class for the MovieDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-movie-detail',
  templateUrl: 'movie-detail.html',
})
export class MovieDetailPage {
  movie: IMovie;
  isFavorite: boolean = false;
  baseurl = "https://image.tmdb.org/t/p/w185";


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private favoriteMovieProvider: FavoriteMovieProvider,
              public alertController: AlertController,
              public toastController: ToastController) {
  }

  ionViewDidLoad() {
    this.movie = this.navParams.data.movie;
    this.isFavorite = this.navParams.data.isFavorite;
  }

  presentToast(msg: string) {
    this.toastController.create({
      message: msg,
      duration: 2000
    }).present();
  }

  toggleFavorite() {
    if (this.isFavorite) {
      const alert = this.alertController.create({
        title: 'Unfavorite?',
        message: 'Are you sure you want to unfavorite this film ?',
        buttons: [{
          text: 'Yes',
          role: 'confirm',
          handler: data => {
            this.isFavorite = !this.isFavorite;
            this.favoriteMovieProvider.toogleFavoriteMovie(this.movie);
            this.presentToast("The film has been deleted from your favorite list");
            this.navCtrl.pop();
          }
        },
          {
            text: 'Cancel',
            role: 'cancel'
          }]
      });
      alert.present();
    } else {
      this.isFavorite = !this.isFavorite;
      this.favoriteMovieProvider.toogleFavoriteMovie(this.movie);
      this.presentToast("The film has been added to your favorite list");
    }
  }
}
