import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {MovieApiProvider} from "../../providers/movie-api/movie-api";


/**
 * Generated class for the AddMoviePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-movie',
  templateUrl: 'add-movie.html',
})
export class AddMoviePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private movieApiProvider: MovieApiProvider, private viewCtrl: ViewController) {
  }

  addMovie(form){
    console.log(form.value);
    this.movieApiProvider.addMovie(form.value).subscribe(data => {
      console.log(data);
    });
    this.closeModal();
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddMoviePage');
  }

}
